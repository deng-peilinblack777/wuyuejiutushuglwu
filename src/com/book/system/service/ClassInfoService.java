package com.book.system.service;
import java.util.List;
import java.util.Map;
public interface ClassInfoService {
    List<Map<String,Object>> getList();
}
