package com.book.system.service;
import java.util.List;
import java.util.Map;
public interface LendListService {
    List<Map<String, Object>> getList(Integer flag);
    int updateById(String serNum);
    int deleteById(String serNum);
    int borrowingBook(Integer id, Integer bookId);
    List<Map<String, Object>> getReaderBookList(Integer id);
    List<Map<String, Object>> getReaderList(Integer id);
    int deleteById(Integer id);
    List<Map<String, Object>> getUnreturnedBooksList(Integer readerId);
    int reportingLoss(Integer serNum);
    List<Map<String, Object>> getReportingLossList(Integer readerId);
    int unhook(Integer serNum);
    int returnBook(Integer serNum);
    int renewalOfLoan(Integer serNum);
}
