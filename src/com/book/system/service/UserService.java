package com.book.system.service;
import com.book.system.entity.User;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
public interface UserService {
    boolean login(User user, HttpSession session);
    String getMenu(Integer flag);
    List<Map<String, Object>> getList(String name, Integer flag);
    int add(User user);
    int updateById(User user);
    int delete(Integer integer);
    int rePassword(User user);
}
