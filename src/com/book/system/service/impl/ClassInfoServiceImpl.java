package com.book.system.service.impl;
import com.book.system.dao.ClassInfoDao;
import com.book.system.service.ClassInfoService;

import java.util.List;
import java.util.Map;
public class ClassInfoServiceImpl implements ClassInfoService {
    private final ClassInfoDao classInfoDao = new ClassInfoDao();
    @Override
    public List<Map<String, Object>> getList() {
        return classInfoDao.getList();
    }
}