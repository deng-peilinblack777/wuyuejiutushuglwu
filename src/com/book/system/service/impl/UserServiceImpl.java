package com.book.system.service.impl;
import com.alibaba.fastjson.JSON;
import com.book.system.dao.UserDao;
import com.book.system.entity.User;
import com.book.system.service.UserService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserDao userDao = new UserDao();
    @Override
    public boolean login(User user, HttpSession session) {
        List<Map<String, Object>> list = userDao.login(user);
        System.out.println("数据库数据: " + list);
        log.info("数据库数据: {}", list);
        if (list != null && list.size() == 1) {
            if (user.getFlag().equals(list.get(0).get("flag"))) {
                Map<String, Object> map = list.get(0);
                User user1 = User.builder()
                        .id((Integer) map.get("id"))
                        .username((String) map.get("username"))
                        .password((String) map.get("password"))
                        .name((String) map.get("name"))
                        .sex((String) map.get("sex"))
                        .birth(LocalDate.parse(String.valueOf(map.get("birth")), DateTimeFormatter.ISO_LOCAL_DATE))
                        .address((String) map.get("address"))
                        .phone((String) map.get("phone"))
                        .flag((Integer) map.get("flag"))
                        .build();
                session.setAttribute("user", user1);
                return true;
            }
        }
        return false;
    }
    @Override
    public String getMenu(Integer flag) {
        String menu = null;
        String resourceName;
        if (flag == 0) {
            resourceName = "json/admin.json";
        } else {
            resourceName = "json/reader.json";
        }
        InputStream in = UserServiceImpl.class.getClassLoader().getResourceAsStream(resourceName);
        if (in == null) {
            System.out.println("无法找到JSON文件。");
            return null;
        } else {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }
                byte[] jsonBytes = baos.toByteArray();
                menu = new String(jsonBytes, StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return menu;
    }
    @Override
    public List<Map<String, Object>> getList(String name, Integer flag) {

        return userDao.getList(name, flag);
    }

    @Override
    public int add(User user) {
        if (user.getFlag() == 0) {
            user.setName("后台管理员");
        } else {
            user.setName("借书者");
        }

        return userDao.save(user);
    }
    @Override
    public int updateById(User user) {
        return userDao.updateById(user);
    }

    @Override
    public int delete(Integer integer) {
        return userDao.delete(integer);
    }

    @Override
    public int rePassword(User user) {
        return userDao.updatePasswordById(user);
    }
}