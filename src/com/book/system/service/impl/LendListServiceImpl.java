package com.book.system.service.impl;
import com.book.system.dao.BookDao;
import com.book.system.dao.LendListDao;
import com.book.system.dao.UserDao;
import com.book.system.entity.LendList;
import com.book.system.service.LendListService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
public class LendListServiceImpl implements LendListService {
    private static final LendListDao lendListDao = new LendListDao();
    private static final BookDao bookDao = new BookDao();
    private static final UserDao userDao = new UserDao();
    @Override
    public List<Map<String, Object>> getList(Integer flag) {
        List<Map<String, Object>> list = lendListDao.getList(flag);
        List<Map<String, Object>> userList = userDao.getList(null, null);
        for (Map<String, Object> map : list) {
            Integer userId = (Integer) map.get("reader_id");
            for (Map<String, Object> userMap : userList) {
                if (userId.equals(userMap.get("id"))) {
                    map.put("userName", userMap.get("username"));
                    break;
                }
            }
        }
        List<Map<String, Object>> bookList = bookDao.getList(null, null);
        for (Map<String, Object> map : list) {
            Integer bookId = (Integer) map.get("book_id");
            for (Map<String, Object> bookMap : bookList) {
                if (bookId.equals(bookMap.get("book_id"))) {
                    map.put("bookName", bookMap.get("name"));
                    break;
                }
            }
        }
        return list;
    }
    @Override
    public int updateById(String serNum) {
        return lendListDao.updateById(serNum);
    }
    @Override
    public int deleteById(String serNum) {
        return lendListDao.deleteById(serNum);
    }
    @Override
    public int borrowingBook(Integer id, Integer bookId) {
        LendList lendList = LendList.builder()
                .readerId(id)
                .flag(0)
                .lossflag(0)
                .lendDate(LocalDate.now())
                .backDate(LocalDate.now().plus(1, ChronoUnit.MONTHS))
                .bookId(bookId)
                .build();
        int i = lendListDao.borrowingBook(lendList);
        if (i > 0) {
            if (bookDao.updateByIdSetNumbet(bookId) > 0) {
                return 1;
            }
        }
        return 0;
    }
    @Override
    public List<Map<String, Object>> getReaderBookList(Integer id) {
        return lendListDao.getReaderBookList(id);
    }
    @Override
    public List<Map<String, Object>> getReaderList(Integer id) {
        return lendListDao.getReaderList(id);
    }
    @Override
    public int deleteById(Integer id) {
        if (lendListDao.deleteById(String.valueOf(id)) > 0) {
            return 1;
        }
        return 0;
    }
    @Override
    public List<Map<String, Object>> getUnreturnedBooksList(Integer readerId) {
        return lendListDao.getUnreturnedBooksList(readerId);
    }
    @Override
    public List<Map<String, Object>> getReportingLossList(Integer readerId) {
        return lendListDao.getReportingLossList(readerId);
    }
    @Override
    public int reportingLoss(Integer serNum) {
        return lendListDao.reportingLoss(serNum);
    }
    @Override
    public int unhook(Integer serNum) {
        return lendListDao.unhook(serNum);
    }
    @Override
    public int returnBook(Integer serNum) {
        List<Map<String, Object>> oneById = lendListDao.getOneById(serNum);
        if (oneById.size() < 1){
            return 0;
        }
        if (lendListDao.returnBook(serNum) > 0) {
            Object bookId = lendListDao.getOneById(serNum).get(0).get("book_id");
            if (bookDao.updateByIdAddNubet(Integer.valueOf(bookId.toString())) > 0) {
                return 1;
            }
        }
        return 0;
    }
    @Override
    public int renewalOfLoan(Integer serNum) {
        List<Map<String, Object>> list = lendListDao.getOneById(serNum);
        if (list.size() < 1) {
            return 0;
        }
        Object backDate = list.get(0).get("back_date");
        LocalDate localDate = LocalDate.parse(backDate.toString());
        LocalDate localDate1 = localDate.plus(1, ChronoUnit.MONTHS);
        if (lendListDao.renewalOfLoan(serNum, localDate1) > 0) {
            return 1;
        }
        return 0;
    }
}