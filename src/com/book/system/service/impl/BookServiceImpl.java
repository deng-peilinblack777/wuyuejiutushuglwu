package com.book.system.service.impl;
import com.book.system.dao.BookDao;
import com.book.system.entity.BookInfo;
import com.book.system.entity.LendList;
import com.book.system.service.BookService;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
/**
 * @author : 邓培霖
 */
public class BookServiceImpl implements BookService {
    private final BookDao bookDao = new BookDao();
    @Override
    public List<Map<String, Object>> getList(String name, Integer classId) {
        List<Map<String, Object>> list = bookDao.getList(name, classId);
        return list;
    }
    @Override
    public int add(BookInfo book) {
        book.setPubDate(LocalDate.now());
        book.setIsbn("9787550252585");
        return bookDao.add(book);
    }
    @Override
    public int updateById(BookInfo book) {
        return bookDao.updateById(book);
    }
    @Override
    public int delete(Integer id) {
        return bookDao.delete(id);
    }
}