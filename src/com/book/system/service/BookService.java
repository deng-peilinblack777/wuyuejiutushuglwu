package com.book.system.service;
import com.book.system.entity.BookInfo;

import java.util.List;
import java.util.Map;
/**
 * @author : 邓培霖
 */
public interface BookService {
    List<Map<String, Object>> getList(String name, Integer classId);
    int add(BookInfo book);
    int updateById(BookInfo book);
    int delete(Integer id);
}
