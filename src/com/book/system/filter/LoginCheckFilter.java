package com.book.system.filter;
import com.alibaba.fastjson.JSON;
import com.book.system.common.R;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * @author 邓培霖
 */
@Slf4j
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
public class LoginCheckFilter implements Filter {
    public boolean check(String[] urls, String uri) {
        for (String url : urls) {
            if (uri.startsWith(url)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, javax.servlet.FilterChain filterChain) throws IOException, ServletException {
        //定义需要放行的url
        String[] urls = new String[]{
                "/css/",
                "/api/",
                "/js/",
                "/img/",
                "/view/",
                "/index.html",
                "/login.html",
                "/user",
        };
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String uri = request.getRequestURI();
        log.info("================URI:" + uri);

        //判断是否属于放行url
        if (check(urls, uri) || "/".equals(uri)) {
            log.info("=================不需要登陆");
            filterChain.doFilter(request, response);
            return;
        }

        //2.判断用户是否登陆
        if (request.getSession().getAttribute("user") != null) {
            log.info("==================用户已经登陆");
            filterChain.doFilter(request, response);
            return;
        }
        //不属于放行，也没有登陆
        log.info("============未登录");
        response.getWriter().write(JSON.toJSONString(R.error(-1, "NOTLOGIN")));
        return;
    }
    @Override
    public void destroy() {

    }
}