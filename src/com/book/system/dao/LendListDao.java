package com.book.system.dao;
import com.book.system.entity.LendList;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
@Slf4j
/**
 * @author : 邓培霖
 */
public class LendListDao {
    public List<Map<String, Object>> getList(Integer flag) {
        StringBuilder tempSql = new StringBuilder("select * from lend_list where 1=1 ");
        if (flag != null && flag != 0) {
            tempSql.append(" and flag = ").append(flag);
        }
        String sql = tempSql.toString();
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql);
    }
    public int updateById(String serNum) {
        String sql = "update lend_list set flag = 3 where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, serNum);
    }

    public int deleteById(String serNum) {
        String sql = "delete from lend_list where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, serNum);
    }
    public int borrowingBook(LendList lendList) {
        String sql = "insert into lend_list(reader_id,book_id,lend_date,back_date,ser_num,lossflag,loss_date,pay_date,flag) " +
                "values(?,?,?,?,?,?,?,?,?)";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, lendList.getReaderId(), lendList.getBookId(),
                lendList.getLendDate(), lendList.getBackDate(), lendList.getSerNum(),
                lendList.getLossflag(), lendList.getLossDate(), lendList.getPayDate(),
                lendList.getFlag());
    }
    public List<Map<String, Object>> getReaderBookList(Integer id) {

        String sql = "select ser_num,reader_id,author,language,publish,name bookName, price ,pay_date,lend_date,back_date " +
                "from lend_list,book_info where lend_list.book_id = book_info.book_id and reader_id = ? and flag != 1 and lossflag != 2";
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql, id);
    }
    public List<Map<String, Object>> getReaderList(Integer id) {
        String sql = "select ser_num,reader_id,author,language,publish,name bookName,price,pay_date,lend_date,back_date from lend_list,book_info where lend_list.book_id = book_info.book_id and reader_id = ? and (flag = 1 or lossflag = 2)";
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql, id);
    }
    public List<Map<String, Object>> getUnreturnedBooksList(Integer readerId) {
        String sql = "select * from lend_list,book_info where lend_list.book_id = book_info.book_id and reader_id = ? and flag != 1 and lossflag != 2";
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql, readerId);
    }
    public List<Map<String, Object>> getReportingLossList(Integer readerId) {
        String sql = "select * from lend_list,book_info where lend_list.book_id = book_info.book_id and reader_id = ? and flag != 1 and lossflag = 1";
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql, readerId);
    }
    public int reportingLoss(Integer serNum) {
        String sql = "update lend_list set lossflag = 1 where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, serNum);
    }
    public int unhook(Integer serNum) {
        String sql = "update lend_list set lossflag = 0 where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, serNum);
    }

    public List<Map<String, Object>> getOneById(Integer id) {
        String sql = "select * from lend_list where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql, id);
    }
    public int renewalOfLoan(Integer serNum, LocalDate backDate) {

        String sql = "update lend_list set back_date = ? where ser_num = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, backDate, serNum);
    }
    public int returnBook(Integer serNum) {
        String sql = "update lend_list set flag=1,lossflag=0,back_date=? where ser_num=?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, LocalDateTime.now(), serNum);
    }
}