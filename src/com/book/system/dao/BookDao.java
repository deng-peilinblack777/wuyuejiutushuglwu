package com.book.system.dao;
import com.book.system.entity.BookInfo;
import com.book.system.entity.LendList;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
@Slf4j
public class BookDao {

    public List<Map<String, Object>> getList(String name, Integer classId) {
        StringBuffer tempSql = new StringBuffer("select * from book_info where 1=1 ");
        if (name != null && !"".equals(name)) {
            tempSql.append(" and name like '%").append(name).append("%'");
        }
        if (classId != null && classId != 0) {
            tempSql.append(" and class_id = ").append(classId);
        }
        String sql = tempSql.toString();
        log.info("sql语句：{}", sql);
        List<Map<String, Object>> list = BaseDao.executeQuery(sql);
        return list;
    }
    public int delete(Integer id) {
        String sql = "delete from book_info where book_id = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, id);
    }
    public int updateById(BookInfo book) {
        String sql = "update book_info set name=?,author=?,publish = ?," +
                "isbn = ?,introduction = ?,language =?,price=?,pub_date=?," +
                "class_id=? ,number =? " +
                "where book_id=?";
        return BaseDao.executeUpdate(sql, book.getName(), book.getAuthor(), book.getPublish(),
                book.getIsbn(), book.getIntroduction(), book.getLanguage(), book.getPrice(),
                book.getPubDate(), book.getClassId(), book.getNumber(), book.getBookId());
    }
    public int updateByIdSetNumbet(Integer bookId) {
        String sql = "UPDATE book_info\n" +
                "SET number = CASE \n" +
                "                 WHEN number > 0 THEN number - 1 \n" +
                "                 ELSE number \n" +
                "               END\n" +
                "WHERE book_id = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, bookId);
    }
    public int updateByIdAddNubet(Integer bookId) {
        String sql = "UPDATE book_info\n" +
                "SET number = CASE \n" +
                "                 WHEN number IS NOT NULL THEN number + 1 \n" +
                "                 ELSE number \n" +
                "               END\n" +
                "WHERE book_id = ?";
        log.info("sql语句：{}", sql);
        return BaseDao.executeUpdate(sql, bookId);
    }
    public int add(BookInfo book) {
        String sql = "insert into book_info(name,author,publish,isbn,introduction,language,price,pub_date,class_id,number) " +
                "values(?,?,?,?,?,?,?,?,?,?)";
        return BaseDao.executeUpdate(sql, book.getName(), book.getAuthor(), book.getPublish(),
                book.getIsbn(), book.getIntroduction(), book.getLanguage(), book.getPrice(),
                book.getPubDate(), book.getClassId(), book.getNumber());
    }
}