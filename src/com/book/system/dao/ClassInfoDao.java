package com.book.system.dao;
import java.util.List;
import java.util.Map;
/**
 * @author : 邓培霖
 */
public class ClassInfoDao {
    public List<Map<String, Object>> getList() {
        String sql = "select * from class_info";
        List<Map<String, Object>> list = BaseDao.executeQuery(sql);
        return list;
    }
}