package com.book.system.dao;
import com.book.system.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
/**
 * @author : 邓培霖
 */
@Slf4j
public class UserDao {
    public List<Map<String, Object>> login(User user) {
        String sql = "select * from user where username = ? and password = ?";
        log.info("sql:{}", sql);
        List<Map<String, Object>> list = BaseDao.executeQuery(sql, user.getUsername(), user.getPassword());
        return list;
    }
    public List<Map<String, Object>> getList(String name, Integer flag) {
        StringBuilder tempSql = new StringBuilder("select * from user where 1=1 ");
        if (name != null && !"".equals(name)) {
            tempSql.append(" and username like '%").append(name).append("%'");
        }
        if (flag != null && flag != 2) {
            tempSql.append(" and flag = ").append(flag);
        }
        String sql = tempSql.toString();
        log.info("sql语句：{}", sql);
        return BaseDao.executeQuery(sql);

    }
    public int save(User user) {
        String sql = "insert into user(name,username,password,flag,address,phone,sex,birth) values(?,?,?,?,?,?,?,?)";
        return BaseDao.executeUpdate(sql, user.getName(), user.getUsername(), user.getPassword(), user.getFlag(), user.getAddress(), user.getPhone(), user.getSex(), user.getBirth());
    }
    public int updateById(User user) {
        String sql = "update user set name = ?,username = ?,password = ?,flag = ?,address = ?,phone = ?,sex = ?,birth = ? where id = ?";
        return BaseDao.executeUpdate(sql, user.getName(), user.getUsername(), user.getPassword(), user.getFlag(), user.getAddress(), user.getPhone(), user.getSex(), user.getBirth(), user.getId());
    }
    public int delete(Integer integer) {
        return BaseDao.executeUpdate("delete from user where id = ?", integer);
    }
    public int updatePasswordById(User user) {
        log.info("修改密码：{}", user);
        return BaseDao.executeUpdate("update user set password = ? where id = ?", user.getPassword(), user.getId());
    }
}