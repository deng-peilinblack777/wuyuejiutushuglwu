package com.book.system.common;


import lombok.Data;

import java.io.Serializable;


@Data
public class R<T> implements Serializable {
    //状态码
    private Integer code;
    //提示信息
    private String msg;
    //数据
    private T data;

    public static <T> R<T> success(T object) {

        R<T> r = new R<T>();
        //将成功的数据存放在data中
        r.data = object;
        r.code = 1;
        return r;
    }

    public static <T> R<T> success(String msg, T object) {

        R<T> r = new R<T>();
        //将成功的数据存放在data中
        r.data = object;
        r.msg = msg;
        r.code = 1;
        return r;
    }

    public static <T> R<T> error(String msg) {
        R<T> r = new R<T>();
        //错误信息添加到信息中
        r.msg = msg;
        r.code = 0;
        return r;
    }

    public static <T> R<T> error(Integer code, String msg) {

        R<T> r = new R<T>();
        //错误信息添加到信息中
        r.msg = msg;
        r.code = code;
        return r;
    }
}