package com.book.system.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClassInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer classId;
    private String className;
}