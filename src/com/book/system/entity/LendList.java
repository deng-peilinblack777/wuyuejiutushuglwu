package com.book.system.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
/**
 * @author : 邓培霖
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LendList implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer serNum;
    private Integer bookId;
    private Integer readerId;
    private LocalDate lendDate;
    private LocalDate backDate;
    private Integer flag;
    private Integer lossflag;
    private LocalDate lossDate;
    private LocalDate payDate;
}