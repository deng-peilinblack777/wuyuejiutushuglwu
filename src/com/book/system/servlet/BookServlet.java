package com.book.system.servlet;
import com.alibaba.fastjson.JSON;
import com.book.system.common.R;
import com.book.system.entity.BookInfo;
import com.book.system.service.BookService;
import com.book.system.service.impl.BookServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
@WebServlet("/book")
public class BookServlet extends BaseServlet {
    private final BookService bookService = new BookServiceImpl();
    public R<List<Map<String, Object>>> getList(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String classId = request.getParameter("classId");
        List<Map<String, Object>> list = bookService.getList(name, Integer.valueOf(classId));
        return R.success(list);
    }
    public R<String> add(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        BookInfo book = JSON.parseObject(requestBody, BookInfo.class);
        bookService.add(book);
        return R.success("添加成功");
    }
    public R<String> update(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        BookInfo book = JSON.parseObject(requestBody, BookInfo.class);
        bookService.updateById(book);
        return R.success("修改成功");
    }
    public R<String> delete(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        bookService.delete(Integer.valueOf(id));
        return R.success("删除成功");
    }
}