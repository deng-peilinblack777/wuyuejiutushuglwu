package com.book.system.servlet;
import com.book.system.common.R;
import com.book.system.service.ClassInfoService;
import com.book.system.service.impl.ClassInfoServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
@WebServlet(name = "ClassInfoServlet", urlPatterns = "/classInfo")
public class ClassInfoServlet extends BaseServlet {
    private final ClassInfoService classInfoService = new ClassInfoServiceImpl();
    public R<List<Map<String, Object>>> getList(HttpServletRequest request, HttpServletResponse response) {
        List<Map<String, Object>> list = classInfoService.getList();
        return R.success(list);
    }
}