package com.book.system.servlet;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.book.system.common.R;
import com.book.system.entity.User;
import com.book.system.service.UserService;
import com.book.system.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
@WebServlet(name = "UserServlet", value = "/user")
@Slf4j
public class UserServlet extends BaseServlet {
    private final UserService userService = new UserServiceImpl();
    public R<String> login(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        User user = JSON.parseObject(requestBody, User.class);
        if (userService.login(user, request.getSession())) {
            return R.success("登陆成功");
        }
        return R.error("登陆失败，请检查用户名、密码和你的身份是否正确");
    }
    public R<String> getMenu(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            String menu = userService.getMenu(user.getFlag());
            if (menu != null && !"".equals(menu)) {
                return R.success(menu);
            }
        }
        return R.error(-1, "获取菜单失败,请尝试重新登陆");
    }
    public R<String> logout(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();
        return R.success("退出成功");
    }
    public R<List<Map<String, Object>>> getList(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String flag = request.getParameter("flag");
        List<Map<String, Object>> list = userService.getList(name, Integer.valueOf(flag));
        return R.success(list);
    }
    public R<String> add(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        User user = JSON.parseObject(requestBody, User.class);
        if (userService.add(user) == 1) {
            return R.success("添加成功");
        }
        return R.error("添加失败");
    }
    public R<String> update(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        User user = JSON.parseObject(requestBody, User.class);
        if (userService.updateById(user) == 1) {
            return R.success("修改成功");
        }
        return R.error("修改失败");
    }
    public R<String> delete(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (userService.delete(Integer.valueOf(id)) == 1) {
            return R.success("删除成功");
        }
        return R.error("删除失败");
    }
    public R<String> rePassword(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        JSONObject jsonObject = JSON.parseObject(requestBody);
        String oldPassword = (String) jsonObject.get("oldPassword");
        String newPassword = (String) jsonObject.get("newPassword");
        if (oldPassword == null || newPassword == null) {
            return R.error("参数错误");
        }
        log.info("修改密码，旧密码：{}，新密码：{}", oldPassword, newPassword);
        User user = (User) request.getSession().getAttribute("user");
        if (user.getPassword().equals(oldPassword)) {
            user.setPassword(newPassword);
            if (userService.rePassword(user) == 1) {
                return R.success("修改成功");
            } else {
                return R.error("修改失败");
            }
        } else {
            return R.error("修改失败，旧密码错误");
        }
    }
    public R<User> getUsrInfo(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            user.setPassword("");
            return R.success(user);
        }
        return R.error(-1, "获取用户信息失败,请尝试重新登陆");
    }
    public R<String> updateUserInfo(HttpServletRequest request, HttpServletResponse response) {
        String requestBody = getRequestBody(request);
        User user = JSON.parseObject(requestBody, User.class);
        if (userService.updateById(user) == 1) {
            HttpSession session = request.getSession();
            User userSession = (User) session.getAttribute("user");
            user.setPassword(userSession.getPassword());
            session.setAttribute("user", user);
            return R.success("修改成功",null);
        }
        return R.error("修改失败");
    }
}
