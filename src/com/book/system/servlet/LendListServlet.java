package com.book.system.servlet;
import com.book.system.common.R;
import com.book.system.entity.User;
import com.book.system.service.LendListService;
import com.book.system.service.impl.LendListServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
@Slf4j
@WebServlet(name = "LendServlet", urlPatterns = "/lendList")
public class LendListServlet extends BaseServlet {
    private static final LendListService lendListService = new LendListServiceImpl();
    public R<List<Map<String, Object>>> getList(HttpServletRequest request, HttpServletResponse response) {
        String flag = request.getParameter("flag");
        List<Map<String, Object>> list = lendListService.getList(Integer.valueOf(flag));
        return R.success(list);
    }
    public R<String> updateById(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        int i = lendListService.updateById(serNum);
        if (i == 1) {
            return R.success("催还成功");
        } else {
            return R.error("催还失败");
        }
    }
    public R<String> deleteById(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        log.info("ser_num:{}", serNum);
        if (lendListService.deleteById(serNum) == 1) {
            return R.success("删除成功");
        } else {
            return R.error("删除失败");
        }
    }
    public R<String> borrowingBook(HttpServletRequest request, HttpServletResponse response) {
        String bookId = request.getParameter("book_id");
        User user = (User) request.getSession().getAttribute("user");
        log.info("当前登陆者id为：{}", user);
        int i = lendListService.borrowingBook(user.getId(), Integer.valueOf(bookId));
        if (i == 1) {
            return R.success("借书成功");
        } else {
            return R.error("借书失败");
        }
    }
    public R<List<Map<String, Object>>> getReaderBookList(HttpServletRequest request, HttpServletResponse response) {

        User user = (User) request.getSession().getAttribute("user");
        log.info("当前登陆者id为：{}", user.getId());
        return R.success(lendListService.getReaderBookList(user.getId()));
    }
    public R<String> returnBook(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        if (lendListService.returnBook(Integer.valueOf(serNum)) == 1) {
            return R.success("还书成功");
        } else {
            return R.error("还书失败");
        }
    }
    public R<String> renewalOfLoan(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        if (lendListService.renewalOfLoan(Integer.valueOf(serNum)) == 1) {
            return R.success("续借成功");
        } else {
            return R.error("续借失败");
        }
    }
    public R<List<Map<String, Object>>> getReaderList(HttpServletRequest request, HttpServletResponse response) {

        User user = (User) request.getSession().getAttribute("user");
        log.info("当前登陆者id为：{}", user.getId());
        return R.success(lendListService.getReaderList(user.getId()));
    }
    public R<List<Map<String, Object>>> getUnreturnedBooksList(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        log.info("当前登陆者id为：{}", user.getId());
        return R.success(lendListService.getUnreturnedBooksList(user.getId()));
    }
    public R<String> reportingLoss(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        if (lendListService.reportingLoss(Integer.valueOf(serNum)) == 1) {
            return R.success("挂失成功");
        } else {
            return R.error("挂失失败");
        }
    }
    public R<List<Map<String, Object>>> getReportingLossList(HttpServletRequest request, HttpServletResponse response) {

        User user = (User) request.getSession().getAttribute("user");
        log.info("当前登陆者id为：{}", user.getId());
        return R.success(lendListService.getReportingLossList(user.getId()));
    }
    public R<String> unhook(HttpServletRequest request, HttpServletResponse response) {
        String serNum = request.getParameter("ser_num");
        if (lendListService.unhook(Integer.valueOf(serNum)) == 1) {
            return R.success("挂失成功");
        } else {
            return R.error("挂失失败");
        }
    }
    public R<String> compensateFor(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }
}