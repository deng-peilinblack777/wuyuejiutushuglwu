package com.book.system.servlet;
import com.alibaba.fastjson.JSON;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Method;
public class BaseServlet extends HttpServlet {
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        HttpServletResponse response = (HttpServletResponse) res;
        req.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String m = req.getParameter("m");
        try {
            Method method = this.getClass()
                    .getDeclaredMethod
                            (m, HttpServletRequest.class,
                                    HttpServletResponse.class);
            Object invoke = method.invoke(this, req, response);
            if (invoke != null) {
                response.getWriter().write(JSON.toJSONString(invoke));
            }

        } catch (NoSuchMethodException e) {
            response.setStatus(404);
            response.getWriter().write("{\"msg\":\"请求错误\"}");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(500);
            response.getWriter().write("{\"msg\":\"服务器繁忙,请稍后再试！\"}");
        }
    }
    public String getRequestBody(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return builder.toString();
    }

}
